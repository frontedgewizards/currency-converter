CurrencyConverter = require  './index.js'

cC = new CurrencyConverter()

cC.getRate('GBP', 'USD')
.then (rate) ->
  console.log "getRate('GBP','USD') resolved with #{rate}"
.catch (error) ->
  console.log "getRate('GBP','USD') rejected with ", error

cC.getRate('CAD', 'USD')
.then (rate) ->
  console.log "1st getRate('CAD','USD') resolved with #{rate}"
.catch (error) ->
  console.log "1st getRate('CAD','USD') rejected with ", error

console.log "2nd getRate('CAD','USD')"
cC.getRate('CAD', 'USD')
.then (rate) ->
  console.log "2nd getRate('CAD','USD') resolved with #{rate}"
.catch (error) ->
  console.log "2nd getRate('CAD','USD') rejected with ", error

console.log "convert(2.00, 'GBP', 'USD')"
cC.convert(2.00, 'GBP', 'USD')
.then (val) ->
  console.log "convert(2.00, 'GBP', 'USD') resolved with #{val}"
.catch (error) ->
  console.log "convert(2.00, 'GBP', 'USD') rejected with ", error
