chai = require 'chai'
chai.should()
chaiAsPromised = require 'chai-as-promised'
chai.use(chaiAsPromised)
sinon = require 'sinon'
sinonChai = require 'sinon-chai'
Promise = require 'bluebird'
require('sinon-as-promised')(Promise)
chai.use(sinonChai)
proxyquire = require 'proxyquire'

FAKE_RATE = 1.54321

describe "CurrencyConverter", ->

  describe "the module", ->
    CurrencyConverter = null
    beforeEach ->
      CurrencyConverter = require '../index.js'

    it "should export a constructor function", ->
      CurrencyConverter.should.be.a('function')

  describe 'CurrencyConstructor instance', ->
    CurrencyConverter = converter = null

    beforeEach ->
      CurrencyConverter = require '../index.js'
      converter = new CurrencyConverter()

    it 'should be an instance of currencyConverter', ->
      converter.should.be.an.instanceof(CurrencyConverter)

    it 'should have a getRate method', ->
      converter.getRate.should.be.a('function')

    it 'should have a convert method', ->
      converter.convert.should.be.a('function')

    describe 'getRate method', ->
      agentStub = CurrencyConverter = converter = null

      beforeEach ->
        agentStub =
          get: sinon.stub().returns({end: sinon.stub().callsArgWith(0, null, {text: '1.54321'})})
        CurrencyConverter = proxyquire '../index.js', {'superagent': agentStub}
        converter = new CurrencyConverter()

      it 'should return a promise that resolves to 1.0 if fromCode equals toCode', ->
        return converter.getRate('USD', 'USD').should.eventually.equal(1.0)

      it 'should fetch rate from web on first request for new conversion', (done) ->
        converter.getRate('GBP', 'USD').should.eventually.equal(FAKE_RATE).then(->
          agentStub.get.should.have.been.calledOnce
          return
          ).should.notify(done)
        return

      it 'should fetch rate from the cache on subsequent request for same conversion', (done) ->
        converter.getRate('CAD', 'USD').should.eventually.equal(FAKE_RATE)
        converter.getRate('CAD', 'USD').should.eventually.equal(FAKE_RATE)
        .then( ->
          agentStub.get.should.have.been.calledOnce
          return
          ).should.notify(done)

    describe 'convert method', ->
      agentStub = CurrencyConverter = converter = null

      beforeEach ->
        agentStub =
          get: sinon.stub().returns({end: sinon.stub().callsArgWith(0, null, {text: '1.54321'})})
        CurrencyConverter = proxyquire '../index.js', {'superagent': agentStub}
        converter = new CurrencyConverter()

      it 'should convert a currency value', ->
        return converter.convert(2.00, 'GBP', 'USD').should.eventually.equal(2.00 * FAKE_RATE)

      it 'should return a currency value unchanged if toCode and fromCode are the same', ->
        return converter.convert(2.00, 'AUD', 'AUD').should.eventually.equal(2.00)


    describe 'error handling on http error', ->
      agentStub = CurrencyConverter = converter = null

      beforeEach ->
        agentStub =
          get: sinon.stub().returns({end: sinon.stub().callsArgWith(0, new Error('Not Found'), {})})
        CurrencyConverter = proxyquire '../index.js', {'superagent': agentStub}
        converter = new CurrencyConverter()

      it 'should reject the returned promise on getRate if web fetch fails',  ->
        converter.getRate('GBP', 'USD').should.eventually.be.rejectedWith('Not Found')

      it 'should reject the returned promise on convert if web fetch fails', ->
        converter.convert(2.00, 'GBP', 'USD').should.eventually.be.rejected

    describe 'error handling on illegal code', ->
      agentStub = CurrencyConverter = converter = null

      beforeEach ->
        agentStub =
          get: sinon.stub().returns({end: sinon.stub().callsArgWith(0, null, {text: 'False'})})
        CurrencyConverter = proxyquire '../index.js', {'superagent': agentStub}
        converter = new CurrencyConverter()

      it 'should reject the returned promise on getRate if illegal currency code is used',  ->
        converter.getRate('GBP', 'XYZ').should.eventually.be.rejectedWith('illegal currency code')

      it 'should reject the returned promise on convert if web fetch fails', ->
        converter.convert(2.00, 'GBP', 'XYZ').should.eventually.be.rejectedWith('illegal currency code')
