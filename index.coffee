lodash = require 'lodash'
Promise = require 'bluebird'
agent = require 'superagent'
asyncCache = require 'async-cache'

loadRate = (codes, callback) ->
  [fromCode, toCode] = codes.split('.')
  requestUrl = "http://currencies.apps.grandtrunk.net/getlatest/#{fromCode}/#{toCode}"
  agent.get(requestUrl)
  .end (err, res) ->
    rate = parseFloat res.text
    if not err and not lodash.isFinite(rate)
      err = new Error('illegal currency code')
    callback(err, parseFloat(res.text))

class CurrencyConverter
  @rateCache = new asyncCache {max: 100, maxAge: 1000 * 60 * 10, load:loadRate}

  constructor: ->

  getRate: (fromCode, toCode) ->
    return new Promise (resolve, reject) =>
      if fromCode is toCode
        resolve(1.0)
      else
        rateCache = @constructor.rateCache
        cacheGet = Promise.promisify(rateCache.get, rateCache)
        cacheKey = "#{fromCode}.#{toCode}"
        ratePromise =  cacheGet(cacheKey)
        ratePromise
        .then (rate) ->
          resolve(rate)
        .catch (error) ->
          reject(error)

  convert: (amount, fromCode, toCode) ->
    return new Promise (resolve, reject) =>
      @getRate(fromCode, toCode)
      .then (rate) ->
        resolve(amount * rate)
      .catch (error) ->
        reject(error)

module.exports = CurrencyConverter
